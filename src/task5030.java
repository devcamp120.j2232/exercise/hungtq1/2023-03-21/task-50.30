import java.util.Arrays; 
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Array;
import java.util.*;

public class task5030 {
    public static void main(String[] args) throws Exception {
        task5030 task = new task5030();
        //Task 1: Kiểm tra giá trị truyền vào có phải chuỗi hay không
        task5030.checkValueString("Devcamp");

        //task 2: Cắt n phần tử đầu tiên của chuỗi
        String str = "Robin Singh";
        int n = 4;
        str = str.substring(0, n);
        System.out.println("Chuoi sau khi cat n phan tu dau tien: " + str+ "\n");

        //stask 3: Chuyển chuỗi thành mảng các từ trong chuỗi
        String strT3 = "Robin Singh";
        String[] words = strT3.split("\\s+");
        System.out.println("Chuyen chuoi thanh mang cac tu trong chuoi: " + Arrays.toString(words) + "\n");

        //task 4:Chuyển chuỗi về định dạng như output
        String inputT4 = "Robin Singh from USA";
        String outputT4 = inputT4.toLowerCase().replace(' ', '-');
        System.out.println(outputT4);

        //task 5: Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu tiên của mỗi từ
        String inputT5 = "JavaScript exercises";
        task5030.removeSpaceInString(inputT5);

        //task 6: Viết hoa chữ cái đầu tiên mỗi từ
        String inputT6 = "js string exercises";
        String[] wordsT6 = inputT6.split(" ");
        StringBuilder resultT6 = new StringBuilder();
        for (String word : wordsT6) {
            resultT6.append(word.substring(0, 1).toUpperCase()).append(word.substring(1)).append(" ");
        }
        System.out.println(resultT6.toString().trim());

        //task 7: Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
        String strT7 = "Ha";
        int nT7 = 10;
        String resultT7 = task.repeatWord(strT7, nT7);
        System.out.println("Lap lai n lan tu 1 tu cho truoc, co khoang trang giua cac lan lap: " + resultT7);

        //task 8: Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó
        String strT8 = "dcresource";
        int nT8 = 2;
        String[] resultT8 = task.splitString(strT8, nT8);
        System.out.println("Dua chuoi thanh mang ma moi phan tu la n ky tu lan luot tu chuoi do: " + Arrays.toString(resultT8));

        //task 9
        String strT9 = "The quick brown fox jumps over the lazy dog";
        String findStr = "the";
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){
            lastIndex = strT9.indexOf(findStr,lastIndex);
            if(lastIndex != -1){
                count ++;
                lastIndex += findStr.length();
            }
        }
        System.out.println(count);

        //task 10: Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác
        String originalString = "0000";
        String stringToReplace = "123";
        int nT10 = 3;
        String resultT10 = replaceRightmostCharacters(originalString, stringToReplace, nT10);
        System.out.println(resultT10);
    }
    //task1
    public static void checkValueString(String s) {
        if(s.getClass().equals(String.class) == true){
            System.out.println("Gia tri truyen vao la chuoi");
        }
        else{
            System.out.println("Gia tri truyen vao khong phai la chuoi");
        }
    }
    //stask 5
    public static void removeSpaceInString(String str){
        String[] words = str.split(" ");
        StringBuilder result = new StringBuilder();
        for (String word : words) {
            result.append(word.substring(0, 1).toUpperCase()).append(word.substring(1));
        }
            System.out.println(result.toString() + "\n");
    }
    //task 7
    public static String repeatWord(String word, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += word + " ";
        }
        return result.trim();
    }
    //task 8: 
    public static String[] splitString(String str, int n) {
        int arrayLength = (int) Math.ceil((double) str.length() / n);
        String[] result = new String[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            int startIndex = i * n;
            int endIndex = Math.min(startIndex + n, str.length());
            result[i] = str.substring(startIndex, endIndex);
        }
        return result;
    }
    //task 10
    public static String replaceRightmostCharacters(String originalString, String stringToReplace, int n) {
        if (n > originalString.length()) {
          return stringToReplace;
        }
        return originalString.substring(0, originalString.length() - n) + stringToReplace;
      }
}
